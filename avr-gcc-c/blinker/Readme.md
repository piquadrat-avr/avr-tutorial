# Blinker

This is an example of programming pin 1 as an output pin and toggle the pin on and off.
The example uses for-loops to create the delay, the delay is not for any specific duration (with 1MHz it's approximately 1s delay for each toggle)


