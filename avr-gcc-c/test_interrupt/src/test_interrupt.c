#include <util/delay.h>
#include <avr/interrupt.h>

#define PIN_PCINT 0
#define LED_INTERRUPT 2
#define LED_BLINKER 1
#define sbi(x,y) x |= (1 << y) // set bit
#define cbi(x,y) x &= ~(1 << y) //clear bit - using bitwise AND operator
#define tbi(x,y) x ^= _BV(y) //toggle bit - using bitwise XOR operator


ISR(PCINT0_vect) {
  sbi(PORTB, LED_INTERRUPT);
}


int main() {
	cli();
	DDRB = ((1 << LED_BLINKER) | (1 << LED_INTERRUPT)); // PORTB1 and PORTB2 as OUTPUT, others as input
	PORTB = (1 << PIN_PCINT); // Set pullup for pin 0, all outoput pins 0
	GIMSK |= (1 << PCIE);   // pin change interrupt enable
	PCMSK |= (1 << PIN_PCINT ); // pin change interrupt enabled for PB0
	sei();                  // enable interrupts
	while(1) { //Infinite loop
		_delay_ms(250);
		tbi(PORTB, LED_BLINKER);
	}
	return 0;
}
