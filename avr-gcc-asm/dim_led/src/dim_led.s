#define SFR_OFFSET 0x20
#define PORTB 0x18
#define DDRB 0x17
#define PCMSK 0x15
#define GIMSK 0x3B
#define GTCCR 0x2C
#define TCCR0B 0x33
#define TCCR0A 0x2A
#define OCR0A 0x29
#define TIMSK 0x39
#define PCIE 5
#define MAX_OFF_CYCLES 8
#define PIN_CHANGE_DELAY 255
#define ON_OFF_PERIODS 8
#define COUNTDOWN_START ON_OFF_PERIODS


.global main
.comm countdown,1
.comm on_periods,1
.comm pinchange_delay,1
.comm counter,1

init:
    ;;; Clear interrupt flag
	cli
    ;;; Enable pin change interrupt (PCIE)
	ldi r16, 0b00100000
	out GIMSK,r16
	ldi r16, 0b00000110
	out DDRB,r16
	sbi PORTB,0
	sbi PCMSK,0

    ;;; Set CTC Modus
  	; in C: TCCR0A = (1<<WGM01); // CTC Modus
	ldi r16,0b00000010
    out TCCR0A,r16
    ;;; Set prescalar to 64 (doc p80)
	; in C: TCCR0B = (1<<CS01) | (1<<CS00); // Prescaler 64
	ldi r16, 0b00000011
	out TCCR0B, r16
    ;;; Set compare register to 125
    ldi r16,12
	out OCR0A,r16
	;;;Set TMS to start timer
    in r16,GTCCR
	andi r16, 0b01111110
	out GTCCR,r16
    in r16,TIMSK
    sbr r16,0b00010000
    out TIMSK,r16
	ldi r16, MAX_OFF_CYCLES
	sts countdown,r16
	sts on_periods,r16
	ldi r16, 0
	sts pinchange_delay,r16
	sei
	ret

; According to ATtiny85_Datasheet.pdf "9.1 Interrupt Vectors in ATtiny25/45/85"
.global __vector_2
__vector_2:
	cli
	push r16
	;;; If pinchange_delay is not zero, decrease it and otherwise ignore the interrupt
    lds r16,pinchange_delay
    cpi r16,0
    brne vector_2_leave

    ;;; pinchange_delay is zero; set it to PIN_CHANGE_DELAY and accept interrupt
    ldi r16,PIN_CHANGE_DELAY
    sts pinchange_delay,r16

	;;; If counter is > 0, decrease it, otherwise wrap it around and set it to ON_OFF_PERIODS
	lds r16,on_periods
	cpi r16,0
    breq vector_2_wrap_counter
    dec r16
	sts on_periods,r16
    rjmp vector_2_leave
vector_2_wrap_counter:
	ldi r16,ON_OFF_PERIODS
	sts on_periods,r16
vector_2_leave:
	pop r16
	sei
	reti


; According to ATtiny85_Datasheet.pdf "9.1 Interrupt Vectors in ATtiny25/45/85"
.global __vector_10
__vector_10:
    ;toggle pin 1 in PORTB
    push r16
    push r17
    push r18
  	;;; If pinchange_delay is > 0, decrease it, else continue at _10
    lds r16,pinchange_delay
    cpi r16,0
    breq vec10_10
    dec r16
    sts pinchange_delay,r16
vec10_10:
  	;;; Check countdown, if it is zero
    lds r16,countdown
    lds r17,on_periods
    cp r16,r17
  	brlo vec10_2_off
vec10_2_on:
	sbi PORTB,2
	rjmp vec10_blink
vec10_2_off:
	cbi PORTB,2
vec10_blink:
    cpi r16,0
    ; if countdown is not zero, leave (vec10_iret)
    brne vec10_iret
    ; otherwise toggle bit 1
    in r16,PORTB
    ldi r17,0b00000010
    eor r16,r17
    out PORTB,r16
 	ldi r16, COUNTDOWN_START
vec10_iret:
    dec r16
    sts countdown,r16
    pop r18
    pop r17
    pop r16
	;sbi PORTB,1
	reti

main:
	rcall init
Start:
	sleep
	rjmp Start
