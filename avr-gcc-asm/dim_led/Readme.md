# Dim LED

This file replicates the dim_led example from the avr-gcc-c folder in assembly.
LEDs are efficiently dimmed by toggling them on and off in high frequency (too fast for the human eye to detect).
This example uses the internal timer and a timer interrupt to toggle the LED, and a pin change interrupt to toggle the brightness (affect the on- and off-intervals)

The example toggles an LED connected to pin 2, the pin change interrupt reacts on level changes on pin 0. Pin 0 is high per internal pull-up resistor and needs to be pulled low to trigger the interrupt.

Used in this example:

* Setting CBC timer
* Implement timer interrupt handler
* Setting PCIE (pin change interrupt enable)
* Implement pin change interrupt handler
