# AVR Tutorial
## Overview

This tutorial provides a short introduction with some examples to programming an ATTiny85 chip. 
The purpose is to do the programming as bare-bone as possible. This, to me, means 

* A C-program which starts with main
* The program consists of one or multiple C files
* A makefile is used to compile and link the target binary
* The commands to create the hex-file are visible

That means, I'm not using the Arduino IDE (since the main function is not visible there, instead programs start implementing a loop function called from somewhere. Also the programs in Arduino IDE usually consist of a single source file instead of an arbitrary amount of source files)
For Assembly-examples, they are based on avr-gcc assembly documentation, but not avr-libc. 

The skills are for the most parts transferable to other avr-chips. The C-examples might work directly for other platforms (adjusting the MMCU and AVRDUDE_TARGET in Makefiles). The assembler-files will probably not work on most other platforms due to different port-, interrupt- and other addresses.

## Test Setup

### Software

The development-environment for me is a Fedora desktop. The packages required are

* make
* avr-gcc
* avrdude
* avr-binutils
* avr-libc
* avr-gdb

As an ide, currently I use eclipse with avr-eclipse plugin; however, the Makefile was created manually. The makefiles
provided require the following sequence to work properly:

	make clean # This is actually optional
	make build
	make flash # At this stage, the chip should be wired for flashing, see below


### Hardware

To run the examples, following parts were used:

* Breadboard
* wires
* ATTiny85
* 2 high resistors (~3k Ohm, reset pull-up and )
* 2 low resistors (330 Ohm, series resistor for LED)
* 2 LEDs
* 2 pushbutton (you can bang blank wires together instead)

To program the ATTiny85 chip:
 
* A USBASP programmer
* Some more wires

They are connected for following use

* The low ohm resistors are connected to pin1 / LED1, pin2 / LED2
* One high ohm resistor connects the reset pin to V++ to keep it high
* Pushbutton T1 connects P0 to ground via a high ohm resistor to trigger pin change interrupt
* Pushbutton T2 connects reset-pin to ground to trigger pin reset

Since all pins are connected via sufficiently high resistors, the USBASP-programmer can be connected to the ATTiny without removing the other wires; when leaving it connected, it can even be used to power the circuit while running the example programs.

Required connections to flash the chip are

* RESET to RESET (pin 1)
* GND to GND (pin 4)
* MOSI to MOSI (pin 5)
* MISO to MISO (pin 6)
* SCK to SCK (pin 7)
* VCC to VCC (pin 8)
